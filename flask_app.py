#----------IMPORTATION-----------
from random import randint
from flask import request, jsonify
import requests
import telepot
import urllib3
import re
from database import app, new_user, get_user, rmv_user, User, commit, ssadd, get_owner


#from database import #needed functions
from database import cal_value, check_if
from database import ico_is_open, is_wave_address
from database import get_balance #to get eth, wave and btc from etherscan, wavego and blockcypher

#for addresses
from database import give_unused, update_bal, get_address, this_address, set_to_used
from database import new_address

#for ico statistics
from database import unconfirmed, confirmed #all are functions
from database import cypher_subscriber # coin, address

#-------------------------------------------------------------------------------
#----------CONFIGURATION--------------------------------------------------------
proxy_url = "http://proxy.server:3128"
telepot.api._pools = {
    'default': urllib3.ProxyManager(proxy_url=proxy_url, num_pools=3, maxsize=10, retries=False, timeout=30),
}
telepot.api._onetime_pool_spec = (urllib3.ProxyManager, dict(proxy_url=proxy_url, num_pools=1, maxsize=1, retries=False, timeout=30))

secret = ""
bot = telepot.Bot('')
bot.setWebhook("https://crypt0gene.pythonanywhere.com/{}".format(secret), max_connections=1)
#------------------------------------------------------------------------------------------------------------------------------------
#---------COMMANDS AND PATTERNS-------------------------------------------------
user_commands = {'start', 'ico', 'btc', 'eth', 'waves',
'cgt_address', 'calc_eth', 'calc_btc', 'calc_waves', 'calc_cgt', 'calc',
'ticket', 'commands', 'balance', 'ico_stat', 'profile',
'cgt_address', 'paymentaddress', "help", 'address', 'apply', 'switch'}

admin_commands = {'msg', 'msg_all', 'msg_admin', 'cls_ticket', 'chk_ticket',
'cuser', 'all_tickets', 'total', 'update', 'confirm_payment', 'doc'} | user_commands

dev_commands ={'add_admin', 'end', 'modify'} | admin_commands

commands = user_commands | admin_commands | dev_commands
#------------------------------------------------------------------
pattern1 = r'^/(?P<command>[a-z_]+)$'

pattern2 = r'^/(?P<command>[a-z_]+) (?P<msg>.+)$'

pattern3 = r'^/(?P<command>[a-z_]+) (?P<num>[0-9]{8,9}) (?P<message>.+)$'

pattern4 = r'^/(?P<command>[a-z]+) (?P<msg>[0-9]+)(?P<coin>btc|eth|waves|cgt)$'
#--------------------------------------------------------------------------------------------
#----------USEFULL FUNCTIONS-----------------------------
def send_to_multi(receivers, message):
    rev = []
    for receiver in receivers:
        try:
            bot.sendMessage(receiver.uid, message)
            rev.append(receiver.uid)
        except:
            pass
    return rev

#----------APP&SCRIPT---------------------------------------------------------------------


#-----------------------------------------------------------------------------------------

@app.route('/')
def hello_world():
    return 'ok'

@app.route('/{}'.format(secret), methods=["POST"])
def cgtbot():
    update = request.get_json()
    if "message" in update and "text" in update["message"]:
        message = update["message"]["text"]
        user_id = update["message"]["chat"]["id"]
        command = 'invalid'
        if "first_name" in update["message"]["chat"]:
            name = update["message"]["chat"]["first_name"]
        else:
            name = "no name"
        message_match_pattern1 = re.match(pattern1, message)
        message_match_pattern2 = re.match(pattern2, message)
        message_match_pattern3 = re.match(pattern3, message)
        message_match_pattern4 = re.match(pattern4, message)
        if message_match_pattern1:
            command = message_match_pattern1.group('command')
        elif message_match_pattern2:
            command = message_match_pattern2.group('command')
        elif message_match_pattern3:
            command = message_match_pattern3.group('command')
        elif message_match_pattern4:
            command = message_match_pattern4.group('command')
        else:
            return "ok"
        dev = 171671406
        if not get_user(dev):
            hh = User(uid=dev, uname=name, utype='dev')
            ssadd(hh)
            commit()

        if command in commands: #else, command not supported
            if command == "start":
                bot.sendMessage(user_id, 'welcome {}. click on /help to get a list of available commands. Also see our /terms_and_conditions'.format(name))
                return 'ok'

            elif command == 'switch' and get_user(user_id): #test this later
                if message_match_pattern1:
                    rmv_user(user_id)
                    bot.sendMessage(user_id, 'restart')
                elif message_match_pattern2:
                    _as = message_match_pattern2.group('msg')
                    if _as == 'user':
                        get_user(user_id).utype = 'user'
                        commit()
                    elif _as == 'admin':
                        get_user(user_id).utype = 'admin'
                        commit()
                    else:
                        _as = '---'
                bot.sendMessage(user_id, 'Your account as been restarted as: {}'.format(_as) )
                return 'ok'

            elif command == 'apply':
                output = 'your application has been sumbitted'
                admin_message = '''
username: {}
uid: {}
                '''.format(name, user_id)
                dev = 171671406
                bot.sendMessage(dev, admin_message)
                bot.sendMessage(user_id, output)
                return 'ok'

            elif command == "calc":  #returns cgt worth of eth
                if message_match_pattern4:
                    coin = message_match_pattern4.group('coin')
                    val = message_match_pattern4.group('msg')
                    if coin == 'eth':
                        output = '{} eth will give {} cgt'.format(val, cal_value('eth', val))
                    elif coin == 'btc':
                        output = '{} btc will give {} cgt'.format(val, cal_value('btc', val))
                    elif coin == 'waves':
                        output = '{} waves will give {} cgt'.format(val, cal_value('waves', val))
                    elif coin == 'cgt':
                        out = cal_value('cgt', val)
                        output = 'cgt: {}, \nbtc: {}, \neth: {}, \nwaves: {}.'.format(out[0], out[1], out[2], out[3])
                    else:
                        output = 'coin not supported'
                    bot.sendMessage(user_id, output)
                    return 'ok'
                else:
                    output = 'invalid format for calc command'
                    return 'ok'

            elif command == "ico_stat": # return total cgt, eth, btc and waves
                pass
#------------------------------------------------------------------
            if not get_user(user_id): #moved down
                #return "pls click on /ico to participate in the ico. By clicking, you agree to our terms and condition"
                pass
#--------------------ADMIN AND DEV commands-------------------------------------------

            elif get_user(user_id).utype == "admin" or get_user(user_id).utype == "dev":
#msg (all, admin), cl_ticket, cuser, total (--all, admin, etc), confirm_payment, doc, chk_ticket, update
                if command == 'msg_all':
                    if message_match_pattern2:
                        message = message_match_pattern2.group('msg')
                        receivers = get_user('all')
                        receiver = send_to_multi(receivers, message)
                        bot.sendMessage(user_id, 'message sent to {} people'.format(len(receiver)))
                        return "ok"

                elif command == 'msg_admin':
                    if message_match_pattern2:
                        message = message_match_pattern2.group('msg')
                        receivers = get_user('admin')
                        receiver = send_to_multi(receivers, message)
                        bot.sendMessage(user_id, 'message sent to {} people'.format(len(receiver)))
                        return 'ok'

                elif command == 'msg':
                    if message_match_pattern3:
                        receiver = message_match_pattern3.group("num")
                        message = message_match_pattern3.group("message")
                        bot.sendMessage(receiver, message)
                        output = "message successfully sent to {}".format(receiver)
                        bot.sendMessage(user_id, output)
                        return 'ok'
                    else:
                        output = "msg command wrongly used. use the format /msg user_id message"
                        bot.sendMessage(user_id, output)
                        return 'ok'
#--****************************************************************************************************
                elif command == 'cls_ticket': #resolve opened issue, send message to opener
                    if message_match_pattern2:
                        ticket_id = message_match_pattern2.group("msg")
                        owner = get_owner('ticket', ticket_id)
                        owner.ticket = 0
                        owner.checking = 0
                        commit()
                        output = 'ticket {} has been resolved and closed'.format(ticket_id)
                        user = get_user(user_id)
                        receivers = [user, owner]
                        send_to_multi(receivers, output)
                        return "ok"
                    else:
                        return "command used incoorectly"

                elif command == 'chk_ticket': #return a single user who has problem
                    if message_match_pattern1:
                        owner = get_owner('check_ticket')
                        if owner:
                            ticket_id = str(owner.ticket)
                            owner.checking = 1
                            commit()
                            output = 'Attend to opened ticket {}'.format(ticket_id)
                            receivers = [user_id, owner]
                            send_to_multi(receivers, output)
                            user_info="""
user name: {}
user id: {}
ticket_id: {}
    """.format(owner.uname, owner.uid, ticket_id)
                            bot.sendMessage(user_id, user_info)
                            return "ok"
                        else:
                            bot.sendMessage(user_id, 'no opened ticket')
                            return 'ok'
                    else:
                        bot.sendMessage(user_id, "command used incoorectly")
                        return 'ok'
                    return "ok"

                elif command == "all_tickets":
                    if message_match_pattern1:
                        return "total number of opened ticket"

                    else:
                        return "ticket command used incorrectly"
                elif command == "cuser":
                    if message_match_pattern2:
                        username = message_match_pattern2.group('msg')
                        return "user details"
                    else:
                        pass

                elif command == "total": #user, ticket,
                    return command
                elif command == 'update':
                    return command
                elif command == "confirm_payment":
                    return command
                elif command == "doc":
                    return 'ok'
#**********************************************************************************************
                elif get_user(user_id).utype == "dev": #only for dev
        #add_admin, end
                    if command == "add_admin":
                        return "admin added"
                    elif command == 'end':
                        pass
                        return 'ico has ended'
                    elif command == 'modify':
                        return 'allow admin to modify something'
                else:
                    pass #use pass # not a develop
#***********************************************************************************************
            if command == "ico": #set step to two ****check if ico has not ended
                #check user not in database
                if not get_user(user_id):
                    new_user(user_id, name)
                    output = '''
Next ==> select your
preferred payment method
/btc


/eth


/waves'''
                    bot.sendMessage(user_id, output)
                    return 'ok'
                else:
                    output = '''
Please, proceed by
selecting your preferred
payment method:
/btc , /eth or /waves'''
                    bot.sendMessage(user_id, output)
                    return 'ok'

            elif not get_user(user_id):
                output = """
pls click on /ico
to participate in the ico.
By clicking, you agree to
our terms and conditions."""
                bot.sendMessage(user_id, output)
                return 'ok'

            if get_user(user_id).ico_action_step == 2: #reduce step by one
                assert get_user(user_id).ico_action_step == 2

                if command == "btc":
                    #result = requests.get('https://eltnegene.pythonanywhere.com/btc_address')
                    #dic = result.json()
                    dic = give_unused('btc')
                    #subscribe to cypher hook
                    for i in dic:
                        payment_address = dic[i]
                    #cypher_subscriber('btc', payment_address)
                    get_user(user_id).payment_wallet_address = payment_address
                    get_user(user_id).payment_method = command
                    get_user(user_id).ico_action_step = 1
                    commit()
                elif command == "eth":
                    #result = requests.get('https://eltnegene.pythonanywhere.com/eth_address')
                    #dic = result.
                    dic = give_unused('eth')
                    #subscribe to cypher hook
                    for i in dic:
                        payment_address = dic[i]
                    #cypher_subscriber('eth', payment_address)
                    get_user(user_id).payment_wallet_address = payment_address
                    get_user(user_id).payment_method = command
                    get_user(user_id).ico_action_step = 1
                    commit()
                elif command == "waves":
                    #result = requests.get('https://eltnegene.pythonanywhere.com/wave_address')
                    #dic = result.json()
                    dic = {'test_address': '3P6bQ7NxJjpL895cuGRyrwsWNP9qHakG3ph'}
                    for i in dic:
                        payment_address = dic[i]
                    get_user(user_id).payment_wallet_address = payment_address
                    get_user(user_id).payment_method = command
                    get_user(user_id).ico_action_step = 1
                    commit()
                else:
                    #if get_user(user_id).utype == "user":
                    output = "pls select your preferred payment method: /btc , /etc or /waves"
                    return 'ok'
                output = """
You have selected {} as
your preferred payment
method.
Submit your wave address
with this format ==>
'/address wave_address'
""".format(command)
                bot.sendMessage(user_id, output)
                return 'ok'

            elif get_user(user_id).ico_action_step == 1:
                if command == "address": #reduce step by two
                    if message_match_pattern2:
                        wave_address = message_match_pattern2.group('msg')
                        if not is_wave_address(wave_address):
                            bot.sendMessage(user_id, 'Submission failed. Invalid wave address')
                            return 'ok'
                        user = get_user(user_id)
                        get_user(user_id).ico_action_step = 0
                        commit()
                        get_user(user_id).cgt_wallet_address = wave_address
                        commit()
                        output = '''
Waves address submitted
successfully. You can
check your details using
/profile . Send your {} to
{} to participate in the
ico.
Your cgt will be credited
to **{}** when your payment is
confirmed. Check your address
below'''.format(user.payment_method, user.payment_wallet_address,
user.cgt_wallet_address)

                    else:
                        output = 'submit your address'
                else:
                    #if get_user(user_id).utype == "user":
                    output = "please, submit your wave address where your cgt will be sent"
                bot.sendMessage(user_id, output)
                bot.sendMessage(user_id, user.payment_wallet_address)
                return 'ok'

    #check if user has completed necessary steps or user is a dev or admin
    #before he can use the below commands
    #
            # other ico commands, *********admin and dev goes down here

    #balance, paymentaddress, profile, ticket, cgt_address
    #assert ico_action_step == 0
            elif get_user(user_id).ico_action_step == 0:
                if command == "balance":
                    user = get_user(user_id)
                    #-----------------------------------------------------

                    coin = user.payment_method
                    if coin == 'btc' or coin == 'eth':
                        try:
                            invested = get_balance(coin, user.payment_wallet_address)
                            unissued = cal_value(coin, invested)
                        except:
                            invested = 1
                            unissued = 1
                    elif coin == 'waves':
                        invested = 0
                        unissued = 0

                    #-----------------------------------------------------
                    output ="""
invested amount: {}{}
unissued cgt balance: {}
issued cgt balance: {}

**Invested amount shows
balance that has been
confirmed by the network.
""".format(invested, coin, unissued, user.issued_balance)
                    bot.sendMessage(user_id, output)
                    return 'ok'
                elif command == "ticket":
                    user = get_user(user_id)
                    if user.ticket:
                        output = "You have an opened ticket"
                    else:
                        user.ticket = randint(222222, 999999)
                        commit()
                        output = '''
A ticket with ticket
id : {} has
been opened. You will
be responded to shortly.'''.format(user.ticket)
                    bot.sendMessage(user_id, output)
                    return 'ok'
                elif command == "profile":
                    u = get_user(user_id)
                    output ="""
payment method: {}
payment address: {}
CGT address: {}
""".format(u.payment_method, u.payment_wallet_address, u.cgt_wallet_address)
                    bot.sendMessage(user_id, output)
                    return 'ok'
                elif command == "paymentaddress":
                    return '/profile /paymentaddress'
                elif command == "cgt_address":
                    return '/profile /cgt_address'
                else:
                    output = 'invalid command'
                    bot.sendMessage(user_id, output)
                    return 'ok'
            else:
                output = 'error'
                bot.sendMessage(user_id, output)
                return 'ok'
    bot.sendMessage(user_id, 'input error')
    return 'ok'

@app.route('/doc')
def cgtdoc():
    return 'ok'

@app.route('/monitor')
def cgtmonitor():
    a = get_user('all')
    d = {}
    for b in a:
        d[b.uid] = [b.uname, b.payment_method, b.payment_wallet_address, b.payment_wallet_balance, b.cgt_wallet_address, b.cgt_wallet_balance]
    return jsonify(d)


notify_pat = r'^(?P<coin>[a-z]{0,3}) (?P<address>[a-zA-Z0-9]{26,45}) (?P<amount>[0-9.]+)$'
modify_pat = r'^(?P<coin>[a-z]{0,3})-(?P<address>[a-zA-Z0-9]{26,45})-(?P<amount>[0-9.]+)$'

@app.route('/addr_app/<pquery>')
def addr_app(pquery):

    if pquery:
        message = pquery
        match_notify_pat = re.match(notify_pat, message)
        match_modify_pat = re.match(modify_pat, message)
        if match_notify_pat: #nofify user that amount was received
            coin = match_notify_pat.group('coin')
            address = match_notify_pat.group('address')
            amount = match_notify_pat.group('amount')
            if get_owner('address', address):
                owner = get_owner('address', address).uid
                output ='''
**{} Payment recieved**.
Your /balance will be
credited when the
payment is confirmed
by the network.'''
                bot.sendMessage(owner, output)
                return 'ok'
        elif match_modify_pat: #modify user's account
            coin = match_notify_pat.group('coin')
            address = match_notify_pat.group('address')
            amount = match_notify_pat.group('amount')
            if get_owner('address', address):
                owner = get_owner('address', address)
                assert coin == owner.payment_method
                owner.payment_wallet_balance += float(amount)
                commit()
        else:
            pass

    return 'ok'



#==================================end of bot=========================================================================================

#==================================API=========================================================================================

@app.route('/btc_address')
def btc_address():
    data = give_unused('btc') #dictionary iid:address
    #subscribe to cypher hook
    for i in data:
        payment_address = data[i]
    #cypher_subscriber('btc', payment_address)
    return jsonify(data)

@app.route('/eth_address')
def eth_address():
    data = give_unused('eth') #dictionary iid:address
    #subscribe to cypher hook
    for i in data:
        payment_address = data[i]
    #cypher_subscriber('eth', payment_address)
    return jsonify(data)

@app.route('/wave_address')
def wave_address():
    data = {'test_address': '3P6bQ7NxJjpL895cuGRyrwsWNP9qHakG3ph'}
    return jsonify(data)

@app.route('/cgt_wave_address')
def cgt_wave_address():
    data = {'cgt_wave_address': '3PLSo4AXwHVsnvYW3JCLqx2FWHoTsHHFnL2'}
    return jsonify(data)

#===============================notification=====================================================================

_block = {'ok':'ok'}
@app.route('/test_cypher', methods=["GET", "POST"])#will receive notification and confirmation from blockcypher
def test_hook():
    global _block
    update = request.get_json()
    """ update in dict, update['output'][0]['value'] gives the value sent, ...[0]['addresses']"""
    address = update['outputs'][0]['addresses'][0]
    value = update['outputs'][0]['value']
    coin = 'eth' if address.startswith('0') else 'btc'
    if 'confirmed' in update: #check address*@, change con bal, wave nd coin
        #address giver
        this_address(address).balance = value
        #bot
        if get_owner('address', address):
            owner = get_owner('address', address)
            owner.payment_wallet_balance = value
        else:
            pass
        #stat

    else:#send mesg to user
        #address giver; do nothing
        #bot; send message to address owner
        if get_owner('address', address):
            owner = get_owner('address', address).uid
            bot.sendMessage(owner, 'Your payment was received')

        #stat
        if coin == 'eth':
            unconfirmed().eth_bal += value

        else:
            unconfirmed().btc_bal += value


    return 'ok'


#=====================================stat=====================================================================
@app.route('/amount_raised')#will provide  api
def amount_raised():
    #unconfirmed().wav_bal = get_balance('waves', '3PLSo4AXwHVsnvYW3JCLqx2FWHoTsHHFnL2')
    to_be_distr = 10000
    #unconfirmed().cgt_bal = to_be_distr - float(cal_value('btc', unconfirmed().btc_bal)) - float(cal_value('waves', unconfirmed().wav_bal)) - float(cal_value('eth', unconfirmed().eth_bal))
    data = {
        'btc_bal':unconfirmed().btc_bal,
        'eth_bal':unconfirmed().eth_bal,
        'wav_bal':unconfirmed().wav_bal,
        'cgt_left':unconfirmed().cgt_bal}

    #tell bot

    return jsonify(data)






