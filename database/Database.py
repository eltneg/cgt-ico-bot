from sys import argv
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from migrate.versioning import api
"""from config import SQLALCHEMY_DATABASE_URI
from config import SQLALCHEMY_MIGRATE_REPO"""
import os.path
import imp
import os
import json

basedir = os.path.abspath(os.path.dirname(__file__))
app = Flask(__name__)
'''
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
app.config['SQLALCHEMY_MIGRATE_REPO'] = os.path.join(basedir, 'db_repository')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')'''

SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://{username}:{password}@{hostname}/{databasename}".format(
    username="crypt0gene",
    password="password",
    hostname="",
    databasename="",
)
app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_MIGRATE_REPO'] = os.path.join(basedir, 'db_repository')
app.config["SQLALCHEMY_POOL_RECYCLE"] = 299
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.Integer, index=True, unique=True)
    uname = db.Column(db.String(64))
    utype = db.Column(db.String(64))
    cgt_wallet_address = db.Column(db.String(120))
    cgt_wallet_balance = db.Column(db.Float)
    payment_method = db.Column(db.String(120))
    payment_wallet_address = db.Column(db.String(120))
    payment_wallet_balance = db.Column(db.Float)
    unissued_balance = db.Column(db.Float)
    issued_balance = db.Column(db.Float)
    ico_action_step = db.Column(db.Integer)
    ticket = db.Column(db.Integer)
    checking = db.Column(db.Integer)

    def __init__(self, uid, uname, utype="user"):
        self.uid = uid
        self.uname = uname
        self.utype = utype
        self.ico_action_step = 2
        self.ticket = 0
        self.checking = 0


    def __repr__(self):
        #return '<User nickname= {}>'.format(self.nickname)
        return '<User uid= {}, uname= {}, cgt= {}/{}, payment_wallet= {}/{}{}, issued/unissued= {}/{} >'.format(
        self.uid, self.uname, self.cgt_wallet_address, self.cgt_wallet_balance, self.payment_wallet_address, self.payment_wallet_balance, self.payment_method, self.issued_balance, self.unissued_balance)

def commit():
    db.session.commit()

def ssadd(_uid):
    db.session.add(_uid)

def new_user(_uid, _uname):
    _uid = User(_uid, _uname)
    db.session.add(_uid)
    commit()

def get_user(_uid):
    '''returns either user details or a list of users'''

    if _uid == 'all':
        return User.query.all()
    elif _uid == 'admin':
        return User.query.filter_by(utype='admin').all()
    elif _uid == 'dev':
        return User.query.filter_by(utype='dev').all()
    elif _uid == 'users':
        return User.query.filter_by(utype='user').all()
    else:
        return User.query.filter_by(uid=_uid).first()
    #dot first or all,


def rmv_user(_uid):
    if get_user(_uid):
        db.session.delete(get_user(_uid))
        commit()

def get_owner(_for=None, ticket_id=None):
    if _for == 'ticket':
        owner = User.query.filter_by(ticket=ticket_id).first()
        return owner
    elif _for == 'check_ticket':
        owner = User.query.filter(User.ticket > 222222, User.checking == 0).first()
        return owner
    elif _for == 'address':
        owner = User.query.filter_by(payment_wallet_address=ticket_id).first()
        return owner
#===========================================================================

class Gene_address(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    iid = db.Column(db.Integer)
    address = db.Column(db.String(40))
    address_type = db.Column(db.String(3))
    used = db.Column(db.Integer)
    balance = db.Column(db.Float(30))


    def __repr__(self):
        return "<User(id={}, iid={}, address={}, address_type={}, used={}, balance={})>".format(
            self.id, self.iid, self.address, self.address_type, self.used, self.balance)


def new_address(_iid, _address, _address_type, _used=0):
    """'stores new address into the database'"""
    if not this_address(_address): #check if address already exist
        _address = Gene_address(iid=_iid,
                    address=_address, address_type=_address_type, used=_used)
        db.session.add(_address)
    commit()


def set_to_used(_address):
    "set address to used"
    if this_address(_address):
        this_address(_address).used = 1
        commit()

def this_address(_address):
    '''useful for dealing with a single address and to check existence'''
    return Gene_address.query.filter_by(address=_address).first()

def get_address(param="all"):
    """'returns json- list'"""
    holder = []
    """'can get all, used, unused, a particular address info'"""
    if param == 'all':
        for addresses in Gene_address.query.all():
            holder.append(addresses)
    elif param =='used':
        for addresses in Gene_address.query.all():
            if addresses.used:
                holder.append(addresses.address)
    elif param =='unused':
        for addresses in Gene_address.query.all():
            if not addresses.used:
                holder.append(addresses.address)
    elif param:
        for addresses in Gene_address.query.all():
            if param == addresses.address:
                holder.append(addresses)
    return holder

def give_unused(addr_type="btc"):
    unused = Gene_address.query.filter_by(address_type = addr_type, used = 0).order_by(Gene_address.iid).first()
    set_to_used(unused.address)
    giv = {unused.iid:unused.address}
    return giv

def update_bal(addr, new_bal):
    """'update wallet balance in the database'"""
    this_address(addr).balance = new_bal

#===============================================================================================
class Stats(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    btc_bal = db.Column(db.Float)
    eth_bal = db.Column(db.Float)
    wav_bal = db.Column(db.Float)
    cgt_bal = db.Column(db.Float)
    end_time = db.Column(db.Integer)

    def __repr__(self):
        return '<Stats id= {}, name= {}, btc_bal= {}, eth_bal= {}, wav_bal= {}, cgt_bal= {}>'.format(self.id,
        self.name, self.btc_bal, self.eth_bal,
         self.wav_bal, self.cgt_bal)


def unconfirmed():
    return Stats.query.filter_by(name='unconfirmed').first()


def confirmed():
    return Stats.query.filter_by(name='confirmed').first()

if __name__ == '__main__':
    script, command = argv

    if command == "c":
        print('db_create_all')
        db.create_all()
        if not os.path.exists(SQLALCHEMY_MIGRATE_REPO):
            api.create(SQLALCHEMY_MIGRATE_REPO, 'database repository')
            api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
        else:
            api.version_control(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, api.version(SQLALCHEMY_MIGRATE_REPO))

    elif command == "m":
        print("db_migrate")
        v = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
        migration = SQLALCHEMY_MIGRATE_REPO + ('/versions/%03d_migration.py' % (v+1))
        tmp_module = imp.new_module('old_model')
        old_model = api.create_model(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
        exec(old_model, tmp_module.__dict__)
        script = api.make_update_script_for_model(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, tmp_module.meta, db.metadata)
        open(migration, "wt").write(script)
        api.upgrade(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
        v = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
        print('New migration saved as ' + migration)
        print('Current database version: ' + str(v))

    elif command == "u":
        print("db_upgrade")
        api.upgrade(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
        v = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
        print('Current database version: ' + str(v))

    elif command == "d":
        api.downgrade(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO, v - 1)
        v = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
        print('Current database version: ' + str(v))

    elif command == "v":
        v = api.db_version(SQLALCHEMY_DATABASE_URI, SQLALCHEMY_MIGRATE_REPO)
        print('Current database version: ' + str(v))

    elif command == 'load':
        with open("/home/crypt0gene/mysite/address/btc_pub.json") as json_file:
            btc_addr = json.load(json_file)

        with open("/home/crypt0gene/mysite/address/eth_pub.json") as json_file:
            eth_addr = json.load(json_file)
#store json to db
        for keys in btc_addr:
            new_address(int(keys), btc_addr[keys], "btc")

        for keys in eth_addr:
            new_address(int(keys), eth_addr[keys], "eth")
    elif command == 'load_stat':
        _unconfirmed = Stats(name= 'unconfirmed', btc_bal=0, eth_bal=0, wav_bal=0, cgt_bal=0, end_time= 0)
        _confirmed = Stats(name= 'confirmed', btc_bal=0, eth_bal=0, wav_bal=0, cgt_bal=0, end_time=0)
        ssadd(_unconfirmed)
        ssadd(_confirmed)
        commit()










