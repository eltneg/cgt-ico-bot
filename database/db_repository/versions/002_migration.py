from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
user = Table('user', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('uid', Integer),
    Column('uname', String(length=64)),
    Column('utype', String(length=64)),
    Column('cgt_wallet_address', String(length=120)),
    Column('cgt_wallet_balance', Float),
    Column('payment_method', String(length=120)),
    Column('payment_wallet_address', String(length=120)),
    Column('payment_wallet_balance', Float),
    Column('unissued_balance', Float),
    Column('issued_balance', Float),
    Column('ico_action_step', Integer),
    Column('ticket', Integer),
    Column('checking', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['user'].columns['checking'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['user'].columns['checking'].drop()
