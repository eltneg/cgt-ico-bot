from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
gene_address = Table('gene_address', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('iid', Integer),
    Column('address', String(length=40)),
    Column('address_type', String(length=3)),
    Column('used', Integer),
    Column('balance', Float(precision=30)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['gene_address'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['gene_address'].drop()
